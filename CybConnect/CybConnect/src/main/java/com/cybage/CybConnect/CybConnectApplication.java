package com.cybage.CybConnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CybConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CybConnectApplication.class, args);
	}

}
